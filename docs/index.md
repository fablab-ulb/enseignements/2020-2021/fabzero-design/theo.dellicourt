#### Bienvenue dans mon univers.


[Voici mon site personnel pour le cours d'Architecture et design de l'Univeristé Libre de Bruxelles.](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt)


## A propos de moi

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/photopropfil.jpg)

Je m'appelle Théo Dellicour. Je suis en deuxième année de master en architecture d'intérieur aux Beaux Arts de Bruxelles.


## Mon parcours

Je suis né à Liège en 1994. J'ai étudié dans cette ville jusqu'à l'obtention de mon bachelier en architecture d'intérieur en 2018, à l'[ESA Saint-Luc](https://www.saint-luc.be/cursus/architecture-dinterieur/). Après cela, j'ai décidé de partir vers de nouvelles aventures en déménageant vers Bruxelles. J'ai étudié durant un an à l'[ENSAV La Cambre](https://www.lacambre.be/fr/formations/architecture-d-interieur) avant de me diriger vers l'[ARBA ESA Beaux-Arts de Bruxelles](https://www.arba-esa.be/fr/options.php?oid=40), où je suis actuellement.

L'architetcture et l'architetcure d'intérieur font réellment partie de moi, c'est une passion.
Je suis inscrit dans ce cours dans le but d'apprendre d'avantage sur les outils numériques, ainsi que pour expérimenter mes idées en tant que designer. J'ai déjà utilisé certains outils numériques  (découpe laser, découpe vinyle, imprimante 3d) et j'ai envie d'en apprendre plus, afin de pouvoir les utiliser au besoin dans mon futur parcours professionnel, ainsi que pour des projets personnels.  
Ces outils permettent de mettre en relation des idées et des expérimentations concrètes, et l'expérimentation c'est ma façon de faire.


## Mes hobbies

J'adore la musique, elle rythme ma vie de tous les jours et je n'ai pas de style de prédilection, tout est bon; jazz, funk, disco, house, techno, bref tout ! J'adore la cuisine et du coup j'adore cuisiner, c'est un échapatoir pour moi.  
Je m'intéresse à la sérigraphie et à la céramique. Ce sont des pratiques très différentes l'une de l'autre, et c'est cela qui est intéressant. Le but serait d'avoir mon propre atelier où je pourrais exprimer ce qu'il y a à l'intérieur de moi-même, avec des outils artistiques et l'architecture d'intérieur d'une autre part. Je pense qu'il n'y a pas de limites dans l'art, et que tout est en relation, c'est pourquoi je préfère avoir plusieurs cordes à mon arc.   
Cette année, dans le cadre de mon mémoire, je m'intéresse à la thématique de l'économie circulaire, et je pense que ce cours peut être en relation avec celle-ci, tant dans les matéralités avec lesquelles nous allons travailler, que par l'utilisation des machines, qui permettent de nombreuses choses.

## Objet choisi

### _Ghost chair_ de Rik Mulder
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/image00001.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/chost%20chair2.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/chost%20chair3.jpg)
##### Description  

Chaise créée en 1980 en une seule pièce de **PMMA** (polyméthacrylate de méthyle, souvent appelé "plexiglas"), qui est un polymère thermoplastique transparent obtenu par polyaddition. Si vous voulez plus d'informations à ce sujet, je vous envoie vers ce [lien](https://ramenetessciences.wordpress.com/2017/05/09/le-polymethylmethacrylate-pmma/).



##### Raisons du choix  

J'ai choisi cette chaise car elle a directement attiré mon regard. Elle m'a retenu par sa **légèreté**, sa **fragilité**, sa **transparence** qui crée de nombreux **reflets**, ces reflets qui envahissent l'espace et qui créent des zones d'**ombre** et de **lumière** très intéressants.   
Elle m'inspire le **rêve**, c'est une chaise sans en être une. Sa forme ressemble à un **drapé figé** dans le temps, un drapé matérialisé par un matériau dur, et non un tissu. C'est tout cela qui m'a mené à choisir cet objet, qui m'a questionné et qui m'a donné envie d'expérimenter; une chaise qui sort des limites de l'objet de base, créant un lien entre l'imaginaire et l'espace, entre la chaise et son environnement.  
Malheureusement, je n'ai trouvé aucune information par rapport à celle-ci (et pourtant j'ai bien cherché).   
Le designer en lui-même, est introuvable sur internet! J'en ai donc déduit que la chaise avait été modélisée par une pratique appalé le thermoformage, le PMMA permet d'être chauffé afin de pouvoir le manipuler, celui-ci devient alors souple et flexible.
