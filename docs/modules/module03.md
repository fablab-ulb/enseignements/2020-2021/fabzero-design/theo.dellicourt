# 3. Impression 3D


Ce module était dédié à l’impression 3D d’après notre modélisation réalisée lors du module 2. Pas de chance pour moi, le covid est passé par là et je n’ai donc pas pu suivre les modules 3 et 4. J’ai donc dû faire l’apprentissage par moi-même, fortement aidé par les membres du Fablab qui ont gentiment prit de leur temps pour m’aiguiller, ainsi qu'avec l'aide du git des autres étudiants.

## Marche à suivre

Première chose, après avoir installé [Prusa Slicer](https://www.prusa3d.com/prusaslicer/). Nous utilisons donc les imprimantes [IMK3S](https://www.prusa3d.com/original-prusa-i3-mk3/). La première chose à faire, c’est importer notre modèle sous le format **STL** ou **OBJ**.  

Après cela, il faut régler la taille souhaitée dans l’onglet de droite, ainsi que la disposition et l’orientation de l’objet.  
Pour pouvoir imprimer correctement, il faut paramétrer les réglages. Voici les réglages que j'ai tuilisé pour arriver à des résultats concluants. Ce sont souvent les mêmes réglages car les objets imprimés se ressemblent fortement.

* **Réglages d’impression**  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage1.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage2.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage3.PNG)

* **Réglages du filament**  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage4.PNG)


* **Réglages d’imprimante**  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage5.PNG)

Ensuite, il suffit de cliquer sur **Découper maintenant**, et si cela vous convient et qu'il n'y a pas d'erreurs, cliquer sur **Export G-code**.
Une fois que cela est fait, on prend la carte SD (déjà dans l'imprimante), et on copie dessus le **G-code** obtenu. Le tour est joué (normalement...)!

## Mon expérience

C’est là que j’ai eu beaucoup de problèmes. En effet, en important mon modèle de mon objet originel (chaise de Rik Mulder), le programme ne reconnaissait pas l’épaisseur et un message survenait. Cela m’est arrivé un nombre de fois incalculable. Ça a été l’étape la plus difficile après la modélisation.  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/Capture.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/Capture2.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusa5.PNG)

J’ai dû retravailler les modèles de nombreuses fois avant d’arriver à un résultat plus ou moins concluant.  
J’ai d’abord rajouté de l’épaisseur au modèle, mais rien n’y faisait. J’ai ensuite prit la décision de ne mettre aucun épaisseur et de jouer avec les réglages dans Prusa pour que l’épaisseur soit celle que j’avais réglée.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/testchaise.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/testchaise1.PNG)

J’ai effectué deux premiers tests:  
Un premier avec une seule couche intérieure. Celui-ci était défectueux car des petits trous apparaissaient sur les couches horizontales. Un deuxième avec trois couches intérieures, qui a été imprimé plus efficacement que l’autre mais avec un temps d’impression plus long (logique).

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00015%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00017%20(2).jpeg)

Après ces essais, j’ai décidé d’essayer à une plus grande échelle, et en changeant de modèle. J’ai ici choisi le modèle créé à partir d’un **siège** et non d’une chaise. Celui-ci étant complètement recouvert du drapé. Il faut savoir que ce qui a également été difficile dans les autres  essais, c’était de ne pas avoir une **forme fermée complètement**, cela m’a donc facilité la tâche.  
Cet essais a été assez concluant, mais il réside un problème: je n’arrive pas à avoir un objet creux, ne reposant pas sur une première plaque de support, impossible à enlever.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00011.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00012.jpeg)

Pour essayer de me rapprocher de la forme initiale, j’ai rechangé mon modèle de base (chaise de Rik Mulder). J’ai dû réparer mon fichier stl dans Prusa, ce qui a eu pour effet de fermer la forme complètement sur les côtés. J'ai réalisé cette réparation en faisant un **clic droit** sur l'objet dans la fenêtre **PrusaSlicer**.  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/Capture3.PNG)


C’est la seule manière que j’ai trouvé pour pouvoir imprimer l’objet en entièreté, et que Prusa reconnaisse mon drapé et l'épaisseur partout. Le point négatif de cette manipulation, c'est que l'application rajoute des "parois" pour fermer la forme sur le socle, cela a donc changé la morphologie de mon objet d'origine.

Voici le résultat. On peut voir que, dù au fait que je n'avais mit qu'**une couche** dans les réglages, l'assise (plan horizontal) n'a pas été réalisée correctement.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00013.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00014%20(2).jpeg)

J'ai donc réésayé en **augmantant l'échelle** du modèle (dans la barre d'outil de droite), en mettant un **remplissage** de **5%**, et **3couches** sur tout le modèle.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00018%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00019%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/image00020%20(2).jpeg)

Voilà où j’en suis pour le moment. Je ne compte pas m’arrêter là car j’aimerai vraiment pouvoir obtenir l’objet de base imprimé correctement. Gwendoline m’a donc expliqué qu’il était possible de réalisé un **Scan 3D** à partir d’un objet réel. Il serait donc possible de recréer la _Ghost chair_ avec un drap sur une chaise, et de le scanner ensuite pour avoir mon modèle en taille réelle sur un fichier en 3D. Je n'ai malheureusement pas exercé ceci étant donné que je suis parti sur une autre direction.
