# 1. Etat de l'art et documentation

L’apprentissage commence par une présentation des deux programmes que nous allons le plus utiliser, à savoir **GIT** et **FUSION 360**.

## **GIT**

Gitlab est une plateforme en ligne où tous les étudiants partagent leurs documentations sur leurs apprentissages. Cette plateforme travaille entre un serveur local (notre ordinateur) et un serveur distant (la plateforme en elle même), elle permet une synchronisation.
Sur cette plateforme, chaque étudiant doit expliquer son parcours d’apprentissage, à travers les succès et les erreurs :
Comment fait-on ?
Où s’est-on documenté ?
Quels outils a-t-on utilisé ?
L’objectif est la création d’un site personnel, directement lié aux plateformes du site [Gitlab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design).

Le principe de git est de modifier/customiser des fichier « markdown » pour changer les features du site web.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/dossieroriginegitlab.PNG)

Afin de réaliser cela, nous avons dû installer [Atom](https://atom.io/), qui est un éditeur de texte en open source, capable de lire et d’interagir sur des fichiers .md.  
Il permet de créer un dossier similaire à celui du serveur distant (gitlab) sur notre ordinateur, et de pouvoir le modifier directement à partir de notre serveur local, pour enfin le pousser, le synchroniser, vers notre site personnel.

Avec atom, nous pouvons directement voir le résultat des changements effectués sur le fichier markdown, grâce au package [atom preview](https://atom.io/packages/markdown-preview-enhanced).   
Nous allons devoir incorporer à notre site des photos, des vidéos, et de l’audio. Atom permet de créer des liens avec les différents fichiers sur notre ordinateur, mais également sur internet, en utilisant des lignes de codes spécifiques.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/atompreviewtool.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/atompreview.PNG)
##### Images  
Elles doivent faire +/- 1000 pixels et doivent être le plus léger possible. Pour cela, nous devons réduire la taille/ la compresser, donc réduire les mega pixels (+/- 500 kb).
Des programmes permettent de faire cela, comme [Photoshop](https://www.photoshop.com/fr)/[GIM](http://www.gimphoto.com/)/[BATCH](https://www.batchphoto.com/)/[GRAPHIC MAGIC](http://www.graphicsmagick.org/). Ces programmes ont différentes fonctions selon nos besoins.
##### Vidéos  
Comme les images, elles doivent être réduites au maximum. Là aussi, nous pouvons disposer de programmes le permettant, comme [FFMPEG](http://ffmpeg.org/).    
L’idéal pour une vidéo, c’est 1min pour 10 mégaoctets.

##### Audio  
C’est également la même chose que pour l’audio ou la vidéo: réduire un maximum
les programmes pour l’audio: [AUDACITY](https://audacity.fr/), [KDLIVE](https://kdenlive.org/fr/), FFMPEG.

#### Mon expérience sur Git  
Il y a deux façons de procéder afin de modifier nos modules de Gitlab, le premier est d'éditer directement sur Gitlab (c'esst clairement le plus facile), mais il y a aussi la possibilité de le faire avec Atom. J'ai d'abord essayé avec la première méthode car elle n'était pas très compliquée à utiliser. J'ai ensuite décidé de changer pour la deuxième, afin de me "compliquer la tâche", mais surtout pour comprendre la façon de faire. Tout d’abord, j’ai installé GIT sur mon ordinateur et je l’ai configuré. Pour configurer Git, j'ai suivi [l'aide](https://courses.cs.washington.edu/courses/cse154/19su/resources/assets/atomgit/windows/) fournie dans le GitLab **FabZero**. Ensuite, le but était de cloner mon dossier reprit sous mon nom sur le site Gitlab, sur mon ordinateur et donc sur Atom, pour pouvoir le modifier.

La première chose à faire sur le site gitlab était de rentrer une clé SSH (épreuve assez compliqué pour un novice en la matière). Cela s'obtient via le terminal Powershell, déjà installé sur mon ordinateur. Après avoir eu la clé, il suffit de la copié sur Gitlab, dans les paramètres de notre profil. La clé se copie automatiquement dans un dossir sur notre ordinateur sous le nom **.ssh**.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/SSHKEY.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/SSHKEY1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/sshKEYGITLAB.PNG)

Ensuite, il faut installer Shell Bash, mais cela dépend de notre ordinateur. Par chance, bash était déjà installé sur mon ordinateur, j'ai juste dù changer quelques paramètres.   
**Paramètres** - **Mise a jour et sécurité** - **Mode développeur**
**Fonctionnalités** - **Activer ou désactiver des fonctionnalités** - **Sous système Linux pour Windows** - Redémarrer l'ordinateur.


Après avoir eu de nombreux problèmes à ce niveau, j’ai finalement cloné mon dossier grâce au lien HTTPS. Mon dossier s’est donc cloné sur mon ordinateur, et dans ATOM directement. De là, j’ai pu modifier mes fichier et les prévisualiser directement.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/clonedossierordi.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/clonedossier.PNG)


## Atom
C'est le programme que j'utilise le plus afin de modifier mon espace GitLab. Cela est assez simple à utiliser.
Avant de commencer, j'ai suivi [les différentes leçons](https://www.markdowntutorial.com/) qui m'ont aidé à répertorier les différents signes permettant d'inclure des textes _en italique_, **en gras**, des titres de différentes tailles, l'importation d'images externes ainsi que des images provenant du net.
Voici un petit récapitulatif en photo, ainsi qu'une explication de mon utilisation d'Atom.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/codegit.PNG)

Petite explication:
-  La barre d'outil de gauche nous montre les différents dossiés de Gitlab (copiés sur mon ordinateur).
- Une fois que l'on ouvre un fichier **.md**, il nous est possible de le prévisualiser avec l'outil **Atom Preview**. Une fenêtre s'ouvre alors, c'est la prévisualisation de notre fichier, c'est le résultat que l'on va obtenir.   
- Une fois que l'on a modifié ce qu'on voulait, il suffit d'entre **CTRL-S** pour enregistrer les modifications.  
- Après cela, il faut cliquer sur le nom du fichier dans la barre de droite, dans **Unstaged changes**.  
- Le fichier va alors se placer dans **Stages changes**.  
- On écrit une description de notre modification dans **Commit message** pour enfin appuyer sur **commit to master**.   
- Une fois que cela est fait, il ne faut pas oublier d'appuer sur **Push** dans la barre en-dessous à droite.


Une fois que nos modules sont réalisés, il faut faire une petite manipulation afin de configurer notre site web.  
Pour cela, nous devons modifier le fichier **mkdocs.yml**. Cela va permettre de changer l'intitulé de notre site, avec notre propre nom, ainsi que le thème de celui-ci. Pour ma part, j'ai opté pour le thème _**slate**_ car cela change des conventions ! Voici un [lien](https://www.mkdocs.org/user-guide/configuration/) qui pourra surement vous aider.

En plus de cela, il nous est demandé d'activer les notifications par email. Cela se fait des **paramètres utilisateur**.  Il faut cocher "_Receive notifications about your own activity_". Ensuite, nous devons remplacer **Global** par **Watch**, dans les emplacements qui nous intéressent.  
Pour vérifier que le site est correcterment modifié et configuré, nous pouvons aller voir dans la barre de gauche, dans l'onglet **CI/CD** - **Jobs**. Si la mise à jour est nommé comme _Passed_, c'est que le site a correctement été modifié.

###### Problème survenu
J'ai une fois eu un message d'erreur à l'ouvertue d'Atom:
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%201/messagerreuratom.PNG)

Cela étant assez étrange, j'ai essayé de résoudre le problème sans y parvenir. J'ai donc juste désinstallé et ré-installé Atom, qui s'est ré-ouvert parfaitement après!

## **Fusion 360**   
Pour ce programme que je n’avais jamais utilisé, nous avons eu une formation nous apprenant les bases de la conception. Nous avons réalisé le cube du fablab en 3D afin de s’exercer. Étant dans une situation compliquée (pas d’ordinateur portable car le mien est en réparation), j’ai dû reproduire l’exercice par après.
Ce programme est assez intuitif et dispose de nombreux tutoriels sur internet. C’est un outil qui match directement avec les exportations pour l’impression 3D.   
Je vais parler de mon expérience avec Fusion plus en détail dans mon [module2](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/blob/master/docs/modules/module02.md).
