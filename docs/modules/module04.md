# 4. Découpe assistée par ordinateur


J’avais déjà utilisé la découpe laser auparavant. Celle-ci était assez différente de celle dont le Fablab dispose, mais les bases restent les mêmes et le fonctionnement reste le même.
Pour les mêmes raisons que le module 3, je n’ai pas su suivre cette formation.
J’ai donc lu les différents git et les différentes aides proposées par les enseignants. Vous trouverez les spécéficités et la manière d'utiliser les machine sur le lien que je pose [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/LASER_cutter.pdf), cela représente la formation de base pour découper à l'aide de ces machines.

## Marche à suivre

- #### Autocad
C'est le programme que j'utilise le plus souvent et avec lequel j'ai le plus facile de travailler. Après avoir réalisés les différentes trames, formes et autre à l'échelle correcte, j'**assigne un calque avec couleur** (rouge par exemple) pour les **gravures**, et une autre (bleu par exemple) pour la **découpe**.  
Enfin, j'**enregistre** mon fichier au format **DXF AutoCAD R12/LT2**.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/Capture1.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/Capture.PNG)


- #### Illustrator
Il faut ouvir le fichier **DXF** enregistré sur AutoCAD, comme expliqué ci-dessus. Ensuite, il suffit juste de vérifier que **les couleurs de calque** soient respectées, et que l'**échelle** est correcte (normalement, il n'y a pas de problèmes avec cela à ce niveau-ci). Après cela, il nous suffit d'**exporter** au format **SVG**.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/Capture2.PNG)


- #### Inkscape
 J'ouvre mon fichier **SVG** enregistré à l'étape précédente. Je change les **propriétés du document** pour avoir la taille souhaitée. A cette étape, mon fichier SVG ne s'importait pas à la bonne échelle. Pour remttre à la taille originale il nous suffit de régler la taille dans la barre d'outil du dessus, en modifiant les données **x et y** par les mesures souhaitées. Pour terminer, on **enregistre une copie** du document fini, afin de pouvoir l'ouvrir dans l'application utilisée pour utiliser la machine.
 ![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/Capture3.PNG)
 ![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/Capture4.PNG)


 - #### Lasersaur - Drive Board app
Cette application est liée à la machine. C'est le programme permettant de visualiser la **taille**, **le parcours**, **le temps**, **les priorités d'impression**, **la gravure et la découpe**. Nous devons ouvrir notre fichier SVG, enregistré sur **Inkscape**. Une fois le fichier ouvert, il faut ajouter des **PASS** en fonction de ce qu'on a fait. Pour respecter l'exemple donné ci-dessus (rouge=gravures et bleu=découpe), j'appuie sur le **+** pour assigner une couleur à une **intensité** et une **vitesse**.  
- Rouge: **F=1700** et **%=15** pour la gravure , et
- Bleu: **F=1500** et **%=45** pour la découpe.   
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00008.jpeg)

  Pour visualiser l'espace où la machine va travailler, on **clique** d'abord sur **(0;0)** pour remttre le laser à l'origine, et ensuite sur les **deux flèches** à coté de l'icône **Run**. Une fois que l'on est satisfait et que cela ne présente pas de problèmes, on appuie sur ce fameux **Run** et c'est parti!  
  Pour des raisons de sécurité, il faut rester sur place pour voir si il n'y a pas de problèmes durant le processus et pouvoir prendre l'extincteur au besoin!

  ###### Précautions avant d'utiliser la machine, ou si nous sommes les premiers à l'utiliser:

- **Tourner le bouton d'arrêt d'urgence** pour allumer la machine. Dans le cas d'un problème, c'est également sur ce bouton qu'on appuie pour arrêter la machine.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00011.jpeg)

- **Allumer le bôitier** qui se trouve en-dessous du bureau

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00009.jpeg)

  - **Allumer les deux extracteurs d'air** situés derrière la machine

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00010.jpeg)

  - **Tourner le piston** situé sur le tuyau à droite de la machine. En faisant cela, le bruit d'extraction apparait: c'est bon signe!
  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00012.jpeg)

## Mon expérience  
La consigne de ce module était de réaliser un **abat-jour**, basé sur notre objet de base, **sans l'ajout de colle ou d'éléments** ne provenant pas de l'utilisation de la machine de découper au laser. J’ai d’abord essayé de rechercher les différents **aspects/notions** et concepts de la _Ghost Chair_.  

Étant donné que celle ci est formée d’**une seule et unique plaque**, j’ai voulu faire de même. L’aspect principal de la chaise réside sur un **drapé**, concept que j’ai voulu explorer également.

Après avoir tiré parti des différentes intentions, je me suis posé la question _«Comment réaliser cela?"_"  

Première chose que j’ai fait, c’est d’**appréhender la matière**, à savoir le **polycarbonate de 0,07 mm**.  
Pour cela, j’ai réalisé différents tests me permettant de voir ce qu’il était possible de faire et de comprendre les propriétés du matériau.  
En partant du principe que je voulais réaliser une sorte de **drapé revisité**, j'ai pensé à **différentes trames**.

J’ai réalisés ces différents tests sur des surface de **18cmx18cm** afin de n’avoir une surface assez grande pour pouvoir me rendre compte de ce qu’il était possible de faire. J’ai utilisé différentes trames. En réfléchissants aux différents systèmes possibles, j’ai reprit les codes du pliage par origami, mais également le **maillage/la triangulation** qu'il est possible de faire sur différentes applications de modélisation 3D, comme **Fusion 360**.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00007.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00008%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00009%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00010%20(2).jpeg)


Après avoir récolté ces tests, j’ai pu me rendre compte de ce qui était intéressant et moins intéressant. Les essais jouant sur la notion d’origami étaient assez concluants car ils rendaient la matière plus flexible que ce qu’elle n’est, et permettent donc de générer des plis interessants. Pour ces essais, j’avais utilisé une trame comme celle reprise par les origamis, que j’ai gravé.

En me basant sur cela, j’ai effectué d’autres tests à plus petites échelles. Pour ces tests, j'ai gardé le **même modèle de base**, mais au lieu de changer l'échelle, j'ai changé **les intensités de gravures** pour voir ce qui était le plus adapté à la conception que j'imaginais.  
Les voici en photos: pour ces essais, j'ai utilisé une **intensité** allant de **5% à 20%**. On peut constater qu'une intensité de 5% n'est surement pas assez pour favoriser le pli, et qu'une intensité de 20% est trop accentuée, et tend à ce que le matériau se casse en le pliant. Cet essai est, néanmois, super intéressant car la matière est complètement **souple**, mais ne permet pas d'avoir une **rigidité** pour faire tenir l'objet.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00006.jpeg)


Après avoir réfléchis à l'objet que je voulais obtenir, j'ai décidé de créer une grande plaque avec une trame assez complexe, qui est gravée avec une **vitesse de 1700**, et une **intensité de 15%**.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00030.jpeg)


Cela a représenté quelques problèmes car la feuille a eu tendance à se déformer d'elle-même. Du coup, la buse du laser touchait presque la feuille, ce qui pouvait entraîner l'apparition de flames. En plus de cela, c'est la trame qui était complè!tement modifiée.

J'ai donc suivi les conseils de Gwen et Hélène, qui m'ont dit de **coller la feuille** à l'aide de **double-face** et de **poids**.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00004.jpeg)
C'est ce que j'ai fais, et ca a plutôt pas mal fonctionné (même si des plis se sont tout de même créés)! Fin du casse tête...
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00016.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00005.jpeg)

Pour terminer, j'ai **plié le résultat dans les deux sens** (sur toute la trame), ce qui m'a permit d'avoir des plis des deux côtés de la feuille tout en gardant une rigidité permettant à la feuille de garder sa forme! Voilà le **résultat final**!
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00001.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00002.jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00003%20(2).jpeg)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%204/image00004%20(2).jpeg)
