# 2. Conception assistée par ordinateur


Pour ce module, nous avons apprit les bases de l’impression 3D. Les imprimantes 3D du Fablab de l’Ulb sont des modèles Prusa (IMK3 S), avec différentes buses: 4 et 6mm.  
Nous avons donc dû installer le programme Prusa Slicer sur notre ordinateur afin de régler les différents paramètres utiles pour l’impression et l’utilisation des imprimantes.  
Ce programme n’est pas simple à utiliser au début, car il y a de nombreuses choses à explorer et à comprendre.

La suite du module était consacrée à la modélisation de notre objet choisi au [Design Museum](http://designmuseum.brussels/)  (la Ghost Chair de Rik Mulder dans mon cas).
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/rendu1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/rendu2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/rendu3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/rendu4.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/rendu5.png)

J'ai eu beaucoup de mal face à cette demande, j'ai d'abord fait des essais sur Fusion mais ceux-ci ne se sont pas avérés concluants. J'ai alors décidé d'essayer sur des programmes que j'avais déjà utilisés afin de pouvoir m'exercer à l'impression 3D. Vous trouverez ici une présentation de ma modélisation dans Fusion (j'y suis ENFIN arrivé après avoir réalisé mes essais sur les différents programmes). Vous trouverez également la démarche que j'ai suivi dans ces programmes.

## Fusion 360
C’est là que les choses se sont compliquées pour moi. En effet, cette chaise est assez complexe à modéliser. En essayant différentes fonctions de Fusion 360, je me suis rendu compte que j’allais avoir du mal.  
J’ai d’abord utiliser l’outil **spline** afin de créer différents plans sur lesquels je choisissais des formes assez aléatoires pour les plis.  
Ce procédé s’est avéré peu concluant, j’ai d’abord eu des problèmes pour retrouver la forme d'origine. Ensuite, j'ai créé des formes assez aléatoires sur différents plans, mais cela s'est avéré trop complexe.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusion.PNG)

J'ai donc changé de méthode: l’outil **sphère**, que j’ai modifié par la suite pour créer des plis.
C’était assez simple à modéliser dans ce cas, mais cela ne représentait en aucun cas une chaise, et cet outil ne permet pas de créer la forme d'une chaise.  
J'ai donc opté pour la création d'une **boite**. J'ai mit celle-ci à l'échelle de l'assise d'une chaise.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube.PNG)

A partir de là, j'ai **subdivisé** l'élément, de manière à avoir le plus de surfaces modifiables.
![]()

J'ai ensuite enlevé les parties qui ne m'intéressaient pour la chaise faite d'une seule et unique plaque.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube1.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube2.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube5.PNG)


Après cela, j'ai surelevé l'arrière de l'objet pour créer le dossier.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube7.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube6.PNG)

A partir de là, j'ai joué avec l'outil **modifier** de manière à avoir des plis. Ce n'était pas toujours facile étant donné la forme très spéciale de la chaise d'origine.
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube4.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube8.PNG)


Après avoir fait de nombreuses manipulations et essais, j'ai quand même réussi à avoir une forme qui m'intéressait. Comme quoi, il ne faut pas désepérer (je peux dire que je l'étais)!
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/fusioncube9.PNG)



## Marvelous designer

Pour m'aider à modéliser l'objet, j'ai réfléchi a quel programme utilisé. En cherchant sur le net, j'ai trouvé un programme permettant de réaliser des habits et beaucoup d'autres choses à partir de tissus: **Marvelous Designer**.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous1.PNG)

J’ai d'abord installé le plugin [Marvelous Designer](https://marvelousdesigner.com/) qui permet de réaliser des drapés le plus fidèlement possible.  
La première chose à faire était de recréer l’objet choisi le plus précisément possible.
Afin de comprendre mieux les otuils à utiliser, j'ai regardé différents tutos, dont [celui-ci](https://www.youtube.com/watch?v=Z1di8iIkDcE) qui explique les bases et qui, à _12:00_, nous montre comment réaliser un drapé sur un objet (une personne dans ce cas-ci).  
Après avoir importé le modèle d’une chaise de base, j’ai déposé un drap dessus, que j’ai modulé par la suite. Il m'a fallu réessayer un bon nombre de fois pour régler les propriétés du tissu. Il y a des millliers de possibilités différentes, que ce soit au niveau de l'élasticité, du poid, de la tension, des dimensions, de la rugosité, etc.  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelousproprietes.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous2.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous3.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous4.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous5.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/marvelous6.PNG)

J’ai ensuite rajouté deux « pins » à l’arrière pour recréer les plis présents sur mon objet d’origine. Il m’a fallu de nombreux essais avant d’en avoir un concluant, pour lequel j’étais assez satisfait. J'ai ensuite importé mon modèle dans **Blender** afin de réaliser des rendus, choses que j'ai également pu faire très facilement avec **Fusion 360**.

Il faut savoir que ce programme est super intéressant si vous voulez réaliser des modèles 3D de lits, fauteuils etc, bref tout ce qui est en tissu et qui a donc des plis! Ca donne du réalisme à vos scènes de rendu d'intérieurs!


## Sketchup  

J’ai décidé de changer la méthode pour avoir une forme différente que celle de base. J’ai installé un Plugin sur Sketchup nommé [Clothworks](https://sketchucation.com/plugin/2053-clothworks).  

J’ai d’abord importé le modèle d’une chaise de base, que j’ai ensuite recouverte d’un drapé, pour ensuite le modéliser et le placer comme je le voulais. Le « problème » avec ce programme, c’est qu’il m’était impossible de réaliser des plis moi-même, il y avait donc un certain hasard dans la conception. C’est une chose qui n’est pas mauvaise en soi en regardant l’objet de base.  

La chaise de Rik Mulder semble être figée aléatoirement (sous certaines conditions), c’est donc un paramètre à reproduire pour la modélisation. J’ai fait différents essais, avec différents éléments de mobilier (chaise, chaise haute, tabouret, siège). J’ai prit le parti de recouvrir l’entièreté de l’objet afin de « pluger » l’objet d’origine, et que celui-ci s'apparente plus à un _"real ghost"_.  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/SKP.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/SKP1.PNG)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%202/SKP2.PNG)
