# Final Project

## Objet final

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/GHOST.png)

### **Ghostchair n1**

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/4.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/5.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/6.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/7.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/8.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/9.png)

La **Ghostchair numéro 1** fait partie de la **Ghostfamily**, une série d'objets usuels et d'éléments de mobilier conçus grâce à l'imprimante 3D. Son drapé revisité lui donne un aspect iréel, imaginaire, sorti d'autre part. L'impression 3D permet de lui donner de multiples apparences, tout comme les autres membres de la famille.  


La série d'objet est basée sur une réinterprétation de la **_Chost Chair_**, présente au Design Museum, réalisée en **1980** par **Rik Mulder**. Celle-ci est conçue en une seule feuille de PMMA (polyméthacrylate de méthyle), chauffé afin d'obtenir cette forme finale.
Inspirante par sa **légèreté**, sa **fragilité**. Sa **transparence** crée de nombreux **reflets**, qui envahissent l'espace et qui créent des zones d'**ombre** et de **lumière** très intéressants.  


![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/CHOSTTTTTT.jpg)

La famille est constituée d'éléments de mobilier et d'objets usuels. Nous retrouvons aux cotés de la GhostChair1 d'autres sièges, des lampes, des vases, des objets plus décoratifs ainsi que des verres et bouteilles. La gamme se veut d'être ouverte à un grand nombre d'objets, tous imprimés en 3D.


![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/OKKKKK.png)

Voici le modèle en [STL](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/blob/747bbc719fc968e4bffd8fc7ad0b3564d2884392/docs/images/GHOSTCHAIR.stl) et au format [Fusion360](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/blob/master/docs/images/GHOSTCHAIR.f3d).
## Recherches

En reprenant les différentes notions et termes invoqués ci-dessus. J'ai décidé de reprendre certains éléments comme **source d'inspiration**.   

Pour moi, l'élément le plus inspirant dans cet objet est ce **drapé**, invoquant l'irréel, le rêve, la legèreté, un moment figé dans le temps et dans l'espace. En retravaillant cette idée, j'ai donc exercé différents essais, avec différents objets, pour visualiser ce qui me touchait le plus, et ce que je voulais réaliser à taille réelle, dans la limite du possible.

Pour cela, j'ai travaillé avec les idées formulées dans les **module 3** et **module 4**. A savoir, la découpe laser dans un premier temps, et l'impression 3d dans un autre temps. Les essais fait avec ces deux outils technologiques m'ont poussés à aller plus loin dans ma réflexion.  
Je vais donc séparer ces deux réflexions, afin d'y voir plus clair et de différencier les deux façons de travailler, même si celles-çi viennent toutes les deux, de la même source d'inspiration.

#### "(...) lorsque l'inspiration arrive sans raison, tout artiste doit la matérialiser avant qu’elle ne s’échappe."


A partir de mon approche personnelle, je sais que **l’inspiration** est un élément essentiel de mon travail et de ma façon de fonctionner. En regardant ce que j’ai produit pour ce cours, cela renforce mon avis sur la question.  
Faudrait-il, dès lors, définir l’inspiration avec mes propres termes ? Je pense que cela ne serait pas une mauvaise chose, étant donné les différents sens que cette notion exprime.
Dans ce cadre, c’est l’inspiration de la racine artistique. Elle découle de nos réflexions, de notre imagination.

L’inspiration provient de notre environnement, de notre éducation, mais également de nos connaissances, de notre apprentissage, de nos contacts et de nos intéractions.  

Elle arrive à n’importe quel moment. On essaye souvent de la solliciter, lorsque l’on doit créer ou réfléchir à un acte ou a une situation. C’est quelque chose qui naît en nous, sans qu’on le force ou qu’on le veuille forcément. Ce n'est pas pour autant que je visualise cela comme un "_souffle divin_".  

On trouve l’inspiration, on cherche l’inspiration, tout comme on le fait pour une chasse au trésor. Sauf que cela n’est pas matériel, cela se passe sous forme d'idée, de visualisation.
La source de notre inspiration, par contre, peut l’être. On s’inspire d’un objet pour en créer un autre, on s’inspire d’un élément de notre environnement pour créer, pour poser une réflexion. Une réflexion sur ce que l’on veut faire, et le fait de mettre cela dans un coin de notre tête nous poussera d'avantage à être inspiré.

L’inspiration est en directe relation avec la référence. On peut prendre inspiration de quelque chose comme référence. La référence est souvent une source d’inspiration. Mais l’inspiration peut également venir d’une certaine « partie » de notre référence, et non pas l’entièreté de celle-ci. Pour illustrer ceci, je vais prendre l’exemple de l’objet que j’ai choisi.


La chaise de Rik Mulder m’a servi de référence par rapport à mon objet « pluger ». Mais, au-delà de ça, elle est une grande source d’inspiration quant à la création de l’objet final et aux possibilités qu'offre la matière ou la forme. Je m’inspire d’un élément de la chaise pour créer un objet différent. Je prend donc mon inspiration à partir de l’objet de référence.
L’objet fini est alors différent que si j’avais « copié/collé » l’objet d’origine. Mais on pourra sûrement voir en ce dernier, l’inspiration de la _Ghost Chair_ que j’ai eu pour le créer.

Je vais scinder mes recherches en deux parties, une consacrée à la découpe laser, pour laquelle j'ai exploré les résistances de la matière, et une partie consacrée à l'impression 3D, qui fait partie intégrante de mon objet final et de mes futures perspectives.


### Découpe laser

En reprenant les différents essais du [module 4](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt/modules/module04/), j'ai décidé de ré-interpreter l'idée de la lampe, réalisée à base de matériaux assez similaires dans leurs compositions. Les plaques utilisées permettent de garder les mêmes propriétés techniques.

Avec des gravures d'intensités différentes, des trames plus ou moins grandes, il est possible de réaliser une composition pouvant servir d'abat-jour ou autre. C'est certte dernière idée que je suis entrain de développer.  
La conception repose sur une composition de plusieurs plaques de polypropylène.
L'approche n'est pas aboutie car j'ai préféré me consacrer totalement à l'impression 3D afin de réaliser ma chaise.
Voici une esquisse d'un résultant pouvant être obtenu.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/lamp.png)

### Impression 3D

L'idée du drapé est restée une évidence pour moi, face à la demande de l'objet final. Les différents moyens que j'avais mis en place par rapport à cela, m'ont donné différentes perspectives, logiques et m'ont permit d'appréhender le travail de différentes façon.  
L'idée de base reste la même: **modéliser un objet en le recouvrant d'un drapé**, et cela avec des techniques différentes, passant par **sketchup** et ses outils, ou par **fusion 360**, qui permet d'autres procédés.

#### Lampe 1 - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/1/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/1/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/1/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/1/5.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/1/4.png)

Le principe, ici, est d'utiliser une autre objet, avec une volumétrie générale plus petite, permettant d'imprimer à taille réelle sans devoir dépenser une fortune (c'est le problème avec une chaise pour adulte, qui pourrait peu-être devenir une chaise pour enfant dans le futur?). L'élément est plein, ce qui ne fonctionnera pas si l'on veut le réaliser dans sa totalité, comme une lampe de table.

Voici un premier essai! Cet essai représente uniquement la forme voulue de la lampe, sans y incruster les élements techniques.


#### Lampe 2 - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/2/essai.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/2/essai2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/2/essai3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/LAMPE/2/essai4.png)
Cet essai repsoe sur le même principe que le premier ci-dessus. La forme est différente, et l'élément lumineux, à savoir l'ampoule, est modélisée afin de comprendre comment cela fonctionnerait. Je vous laisse découvrir çela avec la modélisation 3D. L'élément est creux, ce qui permettra d'inclure les éléments techniques à l'intérieur.



#### Verre - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VERRE/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VERRE/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VERRE/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VERRE/4.png)

Afin de pouvoir réaliser un objet imprimé au Fablab de l'ULB, j'ai décidé de réaliser un verre, reposant sur les mêmes principes que les différents objets exprimés ci-dessus, sur Fusion 360. Celui-ci pourra donc être imprimé directement au Fablab, et exprimera les mêmes concepts que mes différents essais.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VERREBLANC.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VERREBLANC1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VERREBLANC2.png)

D'autres verres:
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VERREROUGE.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VERREBLEU.png)


#### Vase - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VASE/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VASE/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VASE/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/VASE/4.png)

En continuant sur la même lancée, j'ai décidé d'exploiter le même principe que pour la lampe et le reste des objets pour un vase. Cet objet pourrait également être imprimé à taille réelle au Fablab de l'ULB, pourrait se décliner par sa forme, par les différences de couleurs dans les filaments, par son matériaux de base, etc.  
Je remarque, avec tout ces essais, que le fait de décliner les objets sous d'autres formes, devient un jeu pour moi. Je ne pense pas avoir fini avec cela, et je pense qu'il est possible de continuer encore d'avantage afin d'avoir des résultats finis dans leurs totalité. Cela donne beaucoup d'idées pour le futur...  
Voici la première version de vase à laquelle j'ai pensé, cela est, biensûr, modifiable à souhait et sans limites!

D'autres vases:
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VASEBLEU.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VASEBLEU1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VASEROUGE.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VASEROUGE1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/VASEROUGE2.png)
#### Siège 1 - Sketchup

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE/4.png)

En utilisant l'outil **Clothworks**, j'ai fait comme les essais réalisés auparavant, et expliqués dans mon [module3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt/modules/module03/). Ici, j'ai opté pour un siège plutôt qu'une chaise, afin de visualiser le confort nécessaire, mais l'idée reste la même. J'ai d'abord modélisé le siège en question, avant de lui ajouter un drapé.  
Voilà le résultat!  


Et voilà le résulat une fois imprimé à l'échelle 1/10!



#### Siège 2 - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE1/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE1/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE1/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE1/4.png)
L'idée ici, était d'utiliser **Fusion 360**, comme je l'avais fait auparavant, en créant une boite et en modifiant les Splines afin de créer une forme différente, plus organique, s'éloignant un peu d'un simple drapé déposé sur un objet. Cette méthode est également détaillée dans mon [module3](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/theo.dellicourt/modules/module03/).

Voilà à quoi ressemble cet essai!



Je n'ai pas imprimé cet essai car il ne me paraissait pas assez concluant par rapport aux autres.

#### Siège 3 - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE3/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE3/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE3/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE3/4.png)

C'est la même idée que précédemment, en utilisant une structure de base moins **subdivisée**.
C'est grâce à cet essai que j'ai pu me rendre compte de ce qui me plaisait avec ce genre de formes; un aspect plus organique, moins maîtrisé et plus aléatoire.  
Voici un aperçu!



#### Siège 4 - Fusion 360

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/4.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/5.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/SIEGE2/6.PNG)

Cet essai est le plus probant à mon goût, il est le résultat de mes recherches, de mes essais, de mes erreurs et de mes succès. La forme est une forme plus complexe, mais qui laisse entrevoir l'inspiration ou presque l'influence que Rik Mulder a eu sur moi avec sa _Ghost Chair_. Je reviendrai plus tard sur celui-ci, car il est celui que j'ai choisi de réaliser comme objet final, à une échelle **1/1**.  
Voici à quoi il ressemble en modélisation 3D!




L'idée du **fantôme** est également bien présente, modifiée de celle de Rik Mulder. Cela a un coté assez amusant et pragmatique en même temps.  
Voici le résulat à l'impression 3D!

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/GRIS.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/GRIS1.png)

Ce premier essai est complètement rempli et fermé, dans le but de pouvoir l'imprimer à taille réelle, en sachant que le poid exercé par la personne doit revenir sur un appui complet sur le sol.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/BLEU.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/BLEU1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/BLEU2.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/BLEU3.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/BLEU4.png)


Ce deuxième essai est creux et représente la forme finale de l'objet imrpimé à taille réelle.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/TRANSP.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/TRANSP1.png)
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/TRANSP2.png)

Ce troisième essai à été réalisé avec du pet-g au lieu de PLA, et celui-ci est transparent/translucide. Si cet aspect pouvait être imrpimé à taille réelle, il représenterait une réelle connexion entre mon objet de référence et le nouveau.

### Démarche finale

Afin de pouvoir réaliser l'objet à taille réelle, j'ai décidé d'entreprendre des démarches dans le but d'y arriver. J'ai contacté plusieurs entreprises, dont [theoneproject](https://www.theoneproject.eu/) situé à Bruxlles, et qui travaille avec les [colossusprinters](https://colossusprinters.com/).

Ceux-ci ont de nombreuses machines qui peuvent imprimer des objets de grandes tailles. Jetez un coup d'oeil sur le site, ça vaut le détour tellement c'est super impressionnant!

Plusieurs options se sont présentées à moi. La première était d'utiliser une imprimante dotée d'une buse inclinée à 45 degrés. Celle-ci permet l'impression en une seule et unique pièce, ce qui rend l'objet beaucoup moins coûteux. Malheureusement, cette machine n'était pas disponible avant mi-février.

La deuxième option était d'imprimer la chaise en deux parties: une pour le dossier, et une pour l'assise, évitant du coup d'imprimer des ponts impossibles à réaliser sans support. Avec cette méthode, la chaise est alors "soudée" en une partie et recouverte d'un coating qui permet de ne pas voir les défauts en recouvrant l'objet d'une matière soit polie et brtillante, soit non. Il y a donc une multitude de possibilités avec ces procédés, en terme de couleur, de filament et de recouvrement, etc. Cepednant, le devis reçu pour cette option était de 1600€, ce qui m'a posé un gros gros problème.

### Futurs projets

Je veux continuer sur cette lancée et pouvoir vraiment réaliser l'objet à taille réelle. Dans ce sens, je vais continuer de travailler sur les différents objets de la GhostFamily, et tenter de débloquer des fonds afin d'imprimer ma chaise, et le reste si cela est possible.

Affaire à suivre...  

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/FINAL/11.png)
